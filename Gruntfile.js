module.exports = function(grunt) {

// Configure task(s)	
  grunt.initConfig({
	  
	 pkg: grunt.file.readJSON('package.json'),
	 uglify: {
		 build: {
			 src: 'dev/js/*.js',
			 dest: 'assets/js/script.min.js'
		 },
		 dev: {
		   options: {
				beautify: true,
				mangle: false,
				compress: false,
				preserveComments: 'all'
			},
		 	src: 'dev/js/*.js',
			dest: 'assets/js/script.js'
		 
		 }
	 },
	sass: {
		dev: {
			options: {
				outputStyle: 'expanded'
			},
			files: [{
				expand: true,
				cwd: 'dev/css',
				src: ['*.sass'],
				dest: 'assets/css',
				ext: '.css'
			}]
		},
		build: {
			options: {
				outputStyle: 'compressed'
			},
			files: {
				'assets/css/main.min.css' : 'dev/css/main.scss'
			}
		}
	},
	
  imagemin: {                          // Task
    dynamic: {
			options: {                       // Target options
        optimizationLevel: 3,
        svgoPlugins: [{ removeViewBox: false }]
      },
			files: [{
        expand: true,                  // Enable dynamic expansion
        cwd: 'dev/img',                   // Src matches are relative to this path
        src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
        dest: 'assets/img'                  // Destination path prefix
      }]
    }
  },
	jade: {
		compile: {
				options: {
						client: false,
						pretty: true
				},
				files: [ {
					cwd: "templates",
					src: "**/*.jade",
					dest: ".",
					expand: true,
					ext: ".html"
				} ]
		}
	},
		
	watch: {
		js:{
	  	files: ['dev/js/*.js'],
	  	tasks: ['uglify:dev']
  		},
	  css:{
			files: ['dev/css/**/*.sass'],
			tasks: ['sass:dev']
	  },
		options: {
      livereload: true,
    }
  }
	  
  });
//	Load the plugins
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-imagemin');	
	grunt.loadNpmTasks('grunt-contrib-jade');
//	Register task(s)

	grunt.registerTask('default', ['uglify:dev', 'sass:dev']);
	grunt.registerTask('build', ['uglify:build', 'sass:build', 'imagemin']);

	
};